<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <script src="/bower_components/jquery.countdown/dist/jquery.countdown.js"></script>
    <title>SEPARATE PAGE</title>
    <link rel="stylesheet" href="/style.css">
</head>
<style>
    
</style>
<body id="signup-form">    
    <div class="container body">
        <div class="cont-1">
            <div class="logo text-center">
                <a href="https://yesweed.com/" target="_blank">
                    <!-- <img src="{{ asset('emailer/logo-green.png') }}" alt="YesWeed" style="border: 0; display: block;"> -->
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 449.78 117.92"><defs><style>.cls-1{fill:#5d8b3d;}.cls-2{fill:#302d2d;}</style></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1" d="M4.32,56.46l15,35.21L50.88,0l4.2,2L19.36,102.75C15.51,111.27,9.79,117.92,1,117.92v-4.67c6.41,0,10.85-5.6,14-12.36L16.79,97,0,58.44Z"></path><path class="cls-1" d="M94.68,88.62l3.62,2.92A28.67,28.67,0,0,1,73.68,105c-18.09,0-29.3-13.19-29.3-29.41S57,46.26,72.51,46.26s28.12,13.19,28.12,29.29a33.75,33.75,0,0,1-.23,3.5H49.64c1.51,11.9,10.73,21.24,24,21.24A24.09,24.09,0,0,0,94.68,88.62ZM49.4,74.38H95.73C95.15,61.55,85,50.93,72.51,50.93,60.26,50.93,50,61.55,49.4,74.38Z"></path><path class="cls-1" d="M113.82,93.87c6.06,4.44,14.47,6.42,22.17,6.42,6.65,0,19-2.1,19-11.09,0-7.7-9.68-9.45-19.25-11.32-11.44-2.1-22.88-4.43-22.88-15.87,0-11.9,13-15.75,23.11-15.75,7.82,0,15.29,1.75,20.54,6.77l-3.27,3.5c-4.32-4.09-10.62-5.6-17.27-5.6-7.94,0-18.09,2.68-18.09,11.08,0,7.7,9.34,9.22,18.79,11C148.24,75.08,160,77.65,160,89.2c0,12.72-15.64,15.76-24,15.76-8.64,0-18-2.45-24.74-7.12Z"></path><path class="cls-2" d="M247.08,47.78,228.3,104.61h-4.09L208.92,54.43l-15.28,50.18h-4.09l-18.9-56.95,4.55-1.4,16.1,48.9,15.29-48.44h4.67l15.17,48.44,16.22-48.9Z"></path><path class="cls-2" d="M306.48,88.62l3.62,2.92A28.68,28.68,0,0,1,285.47,105c-18.08,0-29.29-13.19-29.29-29.41s12.61-29.29,28.13-29.29,28.12,13.19,28.12,29.29a33.75,33.75,0,0,1-.23,3.5H261.43c1.52,11.9,10.74,21.24,24,21.24A24.1,24.1,0,0,0,306.48,88.62ZM261.2,74.38h46.33c-.58-12.83-10.74-23.45-23.22-23.45C272.05,50.93,261.78,61.55,261.2,74.38Z"></path><path class="cls-2" d="M376,88.62l3.62,2.92A28.68,28.68,0,0,1,355,105c-18.09,0-29.29-13.19-29.29-29.41s12.6-29.29,28.13-29.29S382,59.45,382,75.55a33.75,33.75,0,0,1-.23,3.5H331c1.52,11.9,10.74,21.24,24,21.24A24.1,24.1,0,0,0,376,88.62ZM330.75,74.38h46.33c-.58-12.83-10.74-23.45-23.22-23.45C341.6,50.93,331.33,61.55,330.75,74.38Z"></path><path class="cls-2" d="M444.88,103.21V95.86c-4.67,5.83-12,9.1-20.89,9.1-15.87,0-28.71-13.19-28.71-29.41S408.47,46.26,424,46.26c8.87,0,16.22,3.27,20.89,9.1V27.59h4.9v75.62Zm0-14.82V63c-3.5-7.24-11.67-12-20.89-12-13,0-23.69,11.2-23.69,24.62S411,100.29,424,100.29C433.21,100.29,441.38,95.51,444.88,88.39Z"></path></g></g>
                    </svg>
                </a>
            </div>
            <div class="header text-center">
                <h2>Sell online with Yesweed</h2>
                <p>Trusted by over 400,000 businesses worldwide</p>
            </div>
            <div class="form text-center">            
                <script>
                    hbspt.forms.create({
                        region: "na1",
                        portalId: "20434667",
                        formId: "24d5b3b4-96bb-4fb5-8597-8c05cacd8b7e"
                    });
                </script> 
                <!-- <form>
                    <input type="email" id="email" name="email" required placeholder="Enter your email address">
                    <input type="submit" value="Submit">
                </form> -->
                <p>Try Yesweed free for 14 days. No risk, and no credit card required.</p>
            </div>
        </div>
        <div class="counter text-center cont-2">
             <div id="countdown">
                <div id='tiles'></div>
                <div class="labels">
                    <li>Days</li>
                    <li>Hours</li>
                    <li>Mins</li>
                    <li>Secs</li>
                </div>
            </div>
        </div>
        <div class="cont-3">
            <div class="row main-content">
                <div class="col-12 col-md-4 item">
                    <div class="icon"><i class="fa fa-phone"></i></div>
                    <div class="title">Beautiful themes that are responsive and customizable</div>
                    <div class="content">
                        No design skills needed. You have complete control over the look and feel of your website, from its layout, to content and colors
                    </div>
                </div>
                <div class="col-12 col-md-4 item">
                    <div class="icon"><i class="fa fa-map-marker"></i></div>
                    <div class="title">Pricing as low as $9/month</div>
                    <div class="content">
                        Whether you sell online, on social media, in store, or out of the trunk of your car, Shopify has you covered. Start selling anywhere for just $9/month
                    </div>
                </div>
                <div class="col-12 col-md-4 item">
                    <div class="icon"><i class="fa fa-envelope"></i></div>
                    <div class="title">Trusted by over 400,000 businesses worldwide </div>
                    <div class="content">
                        Shopify handles everything from marketing and payments, to secure checkout and shipping 
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    // COUNTDOWN
    var target_date = new Date().getTime() + (1000*3600*24); // set the countdown date
    var days, hours, minutes, seconds; // variables for time units

    var countdown = document.getElementById("tiles"); // get tag element

    getCountdown();

    setInterval(function () { getCountdown(); }, 1000);

    function getCountdown(){

        // find the amount of "seconds" between now and target
        var current_date = new Date().getTime();
        var seconds_left = (target_date - current_date) / 1000;

        days = pad( parseInt(seconds_left / 86400) );
        seconds_left = seconds_left % 86400;

        hours = pad( parseInt(seconds_left / 3600) );
        seconds_left = seconds_left % 3600;

        minutes = pad( parseInt(seconds_left / 60) );
        seconds = pad( parseInt( seconds_left % 60 ) );

        // format countdown string + set tag value
        countdown.innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>"; 
    }

    function pad(n) {
        return (n < 10 ? '0' : '') + n;
    }

</script>